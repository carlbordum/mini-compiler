import re
from typing import Callable
from typing import Generic
from typing import TypeVar


T = TypeVar("T")
U = TypeVar("U")


class Source:
    def __init__(self, code: str, index: int):
        self.code = code
        self.index = index

    def match(self, regex: str) -> "ParseResult[str] | None":
        r = re.match(regex, self.code[self.index :])

        if not r:
            return None

        source = Source(self.code, self.index + r.end())
        return ParseResult(r.group(), source)

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}(code=..., index={self.index})"


class ParseResult(Generic[T]):
    def __init__(self, value: T, src: Source):
        self.value = value
        self.src = src

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}(value={self.value!r}, src={self.src})"


class Parser(Generic[T]):
    def __init__(self, parse: Callable[[Source], ParseResult[T] | None]):
        self.parse = parse

    def or_(self, parser: "Parser[T] | Parser[U]") -> "Parser[T] | Parser[U]":
        return Parser(lambda src: self.parse(src) or parser.parse(src))

    def bind(self, callback: Callable[[T], "Parser[U]"]) -> "Parser[U]":
        def implementation(src):
            result = self.parse(src)
            if result is None:
                return None
            return callback(result.value).parse(result.src)

        return Parser(implementation)

    def and_(self, parser: "Parser[U]") -> "Parser[U]":
        return self.bind(lambda _: parser)

    def map(self, callback: Callable[[T], U]) -> "Parser[U]":
        return self.bind(lambda value: constant(callback(value)))

    def parse_string(self, string: str) -> T:
        src = Source(string, 0)

        result = self.parse(src)
        if result is None:
            raise Exception("Parse error at index 0")

        index = result.src.index
        if index != len(result.src.code):
            raise Exception(f"Parse error at index {index}")

        return result.value


def zero_or_more(parser: Parser[U]) -> Parser[list[U]]:
    def implementation(src):
        results = []
        while item := parser.parse(src):
            src = item.src
            results.append(item.value)
        return ParseResult(results, src)

    return Parser(implementation)


def maybe(parser: Parser[U] | Parser[None]) -> Parser[U] | Parser[None]:
    return parser.or_(constant(None))


def regexp(regex: str) -> Parser[str]:
    return Parser(lambda src: src.match(regex))


def constant(value: U) -> Parser[U]:
    return Parser(lambda src: ParseResult(value, src))


def error(message: str):
    def implementation(_):
        raise Exception(message)

    return Parser(implementation)


assert regexp(r"hello").or_(regexp("world")).parse_string("world") == "world"
assert (
    regexp(r"[a-z]+")
    .bind(
        lambda word: regexp(r"[0-9]+").bind(lambda digits: constant(f"{digits}{word}"))
    )
    .parse_string("hunter22")
    == "22hunter"
)
