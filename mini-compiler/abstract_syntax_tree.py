from dataclasses import dataclass


emit = print


def _label():
    i = 0
    while True:
        yield f".L{i}"
        i = i + 1


label = _label()


@dataclass
class Environment:
    locals: dict[str, int]
    stack_index: int = 0


@dataclass
class AST:
    def emit(self, env: Environment):
        raise NotImplementedError


@dataclass
class Number(AST):
    value: int

    def emit(self, _):
        emit(f"  ldr r0, ={self.value}")


@dataclass
class Id(AST):
    value: str

    def emit(self, env: Environment):
        try:
            offset = env.locals[self.value]
        except KeyError:
            raise Exception(f"Undefined variable: {self.value}")
        emit(f"  ldr r0, [fp, #{offset}]")


@dataclass
class Not(AST):
    term: AST

    def emit(self, env):
        self.term.emit(env)
        emit("  cmp r0, #0")
        emit("  moveq r0, #1")
        emit("  movne r0, #0")


@dataclass
class Equal(AST):
    left: AST
    right: AST

    def emit(self, env):
        self.left.emit(env)
        emit("  push {r0, ip}")
        self.right.emit(env)
        emit("  pop {r1, ip}")
        emit("  cmp r0, r1")
        emit("  moveq r0, #1")
        emit("  movne r0, #0")


@dataclass
class NotEqual(AST):
    left: AST
    right: AST

    def emit(self, env):
        self.left.emit(env)
        emit("  push {r0, ip}")
        self.right.emit(env)
        emit("  pop {r1, ip}")
        emit("  cmp r0, r1")
        emit("  movne r0, #1")
        emit("  moveq r0, #0")


@dataclass
class Add(AST):
    left: AST
    right: AST

    def emit(self, env):
        self.left.emit(env)
        emit("  push {r0, ip}")
        self.right.emit(env)
        emit("  pop {r1, ip}")
        emit("  add r0, r1, r0")


@dataclass
class Subtract(AST):
    left: AST
    right: AST

    def emit(self, env):
        self.left.emit(env)
        emit("  push {r0, ip}")
        self.right.emit(env)
        emit("  pop {r1, ip}")
        emit("  sub r0, r1, r0")


@dataclass
class Multiply(AST):
    left: AST
    right: AST

    def emit(self, env):
        self.left.emit(env)
        emit("  push {r0, ip}")
        self.right.emit(env)
        emit("  pop {r1, ip}")
        emit("  mul r0, r1, r0")


@dataclass
class Divide(AST):
    left: AST
    right: AST

    def emit(self, env):
        self.left.emit(env)
        emit("  push {r0, ip}")
        self.right.emit(env)
        emit("  pop {r1, ip}")
        emit("  udiv r0, r1, r0")


@dataclass
class Call(AST):
    callee: str
    args: list[AST]

    def emit(self, env):
        count = len(self.args)
        if count == 0:
            emit(f"  bl {self.callee}")
        elif count == 1:
            self.args[0].emit(env)
            emit(f"  bl {self.callee}")
        elif count <= 4:
            emit("  sub sp, sp, #16")
            for i, arg in enumerate(self.args):
                arg.emit(env)
                emit(f"  str r0, [sp, #{4 * i}]")
            emit("  pop {r0, r1, r2, r3}")
            emit(f"  bl {self.callee}")
        else:
            raise Exception("More than 4 arguments are not supported")


@dataclass
class Return(AST):
    term: AST

    def emit(self, env):
        self.term.emit(env)
        emit("  mov sp, fp")
        emit("  pop {fp, pc}")


@dataclass
class Block(AST):
    statements: list[AST]

    def emit(self, env):
        for statement in self.statements:
            statement.emit(env)


@dataclass
class If(AST):
    conditional: AST
    consequence: AST
    alternative: AST

    def emit(self, env):
        false_label = next(label)
        end_label = next(label)

        self.conditional.emit(env)
        emit("  cmp r0, #0")
        emit(f"  beq {false_label}")
        self.consequence.emit(env)
        emit(f"  b {end_label}")
        emit(f"{false_label}:")
        self.alternative.emit(env)
        emit(f"{end_label}:")


@dataclass
class Function(AST):
    name: str
    params: list[str]
    body: Block

    def emit(self, _):
        if len(self.params) > 4:
            raise Exception("More than 4 params is not supported")

        emit("")
        emit(f".global {self.name}")
        emit(f"{self.name}:")
        self.emit_prologue()
        env = self.setup_environment()
        self.body.emit(env)
        self.emit_epilogue()

    def setup_environment(self):
        return Environment(
            {param: 4 * i - 16 for i, param in enumerate(self.params)}, -20
        )

    @staticmethod
    def emit_prologue():
        emit("  push {fp, lr}")
        emit("  mov fp, sp")
        emit("  push {r0, r1, r2, r3}")

    @staticmethod
    def emit_epilogue():
        emit("  mov sp, fp")
        emit("  mov r0, #0")
        emit("  pop {fp, pc}")


@dataclass
class Var(AST):
    name: str
    value: AST

    def emit(self, env):
        self.value.emit(env)
        emit("  push {r0, ip}")
        env.locals[self.name] = env.stack_index - 4
        env.stack_index -= 8


@dataclass
class Assignment(AST):
    name: str
    value: AST

    def emit(self, env):
        self.value.emit(env)
        try:
            offset = env.locals[self.name]
        except KeyError:
            raise Exception(f"Undefined variable: {self.name}")
        emit(f"  str r0, [fp, #{offset}]")


@dataclass
class While(AST):
    conditional: AST
    body: AST

    def emit(self, env):
        loop_start = next(label)
        loop_end = next(label)

        emit(f"{loop_start}:")
        self.conditional.emit(env)
        emit("  cmp r0, #0")
        emit(f"  beq {loop_end}")
        self.body.emit(env)
        emit(f"  b {loop_start}")
        emit(f"{loop_end}:")


@dataclass
class Boolean(AST):
    value: bool

    def emit(self, _):
        if self.value:
            emit("  mov r0, #1")
        else:
            emit("  mov r0, #0")


@dataclass
class Undefined(AST):
    def emit(self, _):
        emit("  mov r0, #0")


@dataclass
class Null(AST):
    def emit(self, _):
        emit("  mov r0, #0")


@dataclass
class Array(AST):
    elements: list[AST]

    def emit(self, env):
        length = len(self.elements)

        # Allocate memory for the array. We add 1 more, because we use the
        # first 32-bit to represent the length itself at runtime.
        emit(f"  ldr r0, ={4 * (length + 1)}")
        emit("  bl malloc")

        # r4 is call-preserved. Preserve it, and then use it to store the
        # pointer returned by malloc.
        emit("  push {r4, ip}")
        emit("  mov r4, r0")

        # Store length in the first word of the allocated memory.
        emit(f"  ldr r0, ={length}")
        emit("  str r0, [r4]")

        for i, element in enumerate(self.elements):
            element.emit(env)
            # Move the result of the emitted element assembly at the proper
            # index in our new array.
            emit(f"  str r0, [r4, #{4 * (i + 1)}]")

        # Return pointer in r0 and restore r4.
        emit("  mov r0, r4")
        emit("  pop {r4, ip}")


@dataclass
class ArrayLookup(AST):
    array: AST
    index: AST

    def emit(self, env):
        # Store array index in r0 and array pointer in r1.
        self.array.emit(env)
        emit("  push {r0, ip}")
        self.index.emit(env)
        emit("  pop {r1, ip}")

        emit("  ldr r2, [r1], #4")
        emit("  cmp r0, r2")
        emit("  movhs r0, #0")  # return 0 if out-of-count.
        emit("  ldrlo r0, [r1, r0, lsl #2]")


@dataclass
class Length(AST):
    array: AST

    def emit(self, env):
        self.array.emit(env)
        emit("  ldr r0, [r0, #0]")
