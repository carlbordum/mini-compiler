from functools import reduce
from typing import Callable

import abstract_syntax_tree as ast
from abstract_syntax_tree import AST
from peg import Parser
from peg import zero_or_more
from peg import maybe
from peg import regexp
from peg import constant
from peg import error


def token(pattern):
    return regexp(pattern).bind(lambda value: ignored.and_(constant(value)))


FUNCTION = token(r"function\b")
IF = token(r"if\b")
ELSE = token(r"else\b")
RETURN = token(r"return\b")
VAR = token(r"var\b")
WHILE = token(r"while\b")

COMMA = token(r",")
SEMICOLON = token(r";")
LEFT_PAREN = token(r"\(")
RIGHT_PAREN = token(r"\)")
LEFT_BRACE = token(r"\{")
RIGHT_BRACE = token(r"\}")
LEFT_BRACKET = token(r"\[")
RIGHT_BRACKET = token(r"\]")

ID = token(r"[a-zA-Z_][a-zA-Z0-9_]*")

NUMBER = token(r"[0-9]+").map(lambda n: ast.Number(int(n)))
TRUE = token(r"true\b").map(lambda _: ast.Boolean(True))
FALSE = token(r"false\b").map(lambda _: ast.Boolean(False))
id = ID.map(lambda x: ast.Id(x))
NOT = token(r"!").map(lambda _: ast.Not)
EQUAL = token(r"==").map(lambda _: ast.Equal)
NOT_EQUAL = token(r"!=").map(lambda _: ast.NotEqual)
ASSIGN = token(r"=").map(lambda _: ast.Assignment)
PLUS = token(r"\+").map(lambda _: ast.Add)
MINUS = token(r"-").map(lambda _: ast.Subtract)
STAR = token(r"\*").map(lambda _: ast.Multiply)
SLASH = token(r"/").map(lambda _: ast.Divide)

whitespace = regexp(r"[ \n\r\t]+")
comments = regexp(r"//.*")  # TODO multiline comments, needs regex type
ignored = zero_or_more(whitespace.or_(comments))

expression: Parser[AST] = error("expression parser used before definition")
args: Parser[list[AST]] = expression.bind(
    lambda arg: zero_or_more(COMMA.and_(expression)).bind(
        lambda rest: constant([arg, *rest])
    )
).or_(constant([]))

call: Parser[AST] = ID.bind(
    lambda callee: LEFT_PAREN.and_(
        args.bind(
            lambda args: RIGHT_PAREN.and_(
                constant(
                    ast.Length(args[0])
                    if callee == "length"
                    else ast.Call(callee, args)
                )
            )
        )
    )
)

array: Parser[AST] = LEFT_BRACKET.and_(
    args.bind(lambda args: RIGHT_BRACKET.and_(constant(ast.Array(args))))
)
array_lookup: Parser[AST] = ID.bind(
    lambda name: LEFT_BRACKET.and_(
        expression.bind(
            lambda index: RIGHT_BRACKET.and_(
                constant(ast.ArrayLookup(ast.Id(name), index))
            )
        )
    )
)

boolean: Parser[AST] = TRUE.or_(FALSE)
null: Parser[AST] = token(r"null\b").map(lambda _: ast.Null())
undefined: Parser[AST] = token(r"undefined\b").map(lambda _: ast.Undefined())
scalar: Parser[AST] = boolean.or_(null).or_(undefined).or_(id).or_(NUMBER)

atom: Parser[AST] = (
    call.or_(array)
    .or_(array_lookup)
    .or_(scalar)
    .or_(LEFT_PAREN.and_(expression).bind(lambda e: RIGHT_PAREN.and_(constant(e))))
)
unary: Parser[AST] = maybe(NOT).bind(
    lambda b: atom.map(lambda term: ast.Not(term) if b else term)
)


def infix(
    operator_parser: Callable[[AST, AST], AST],
    term_parser: Parser[AST],
):
    return term_parser.bind(
        lambda term: zero_or_more(
            operator_parser.bind(
                lambda operator: term_parser.bind(
                    lambda term: constant((operator, term))
                )
            )
        ).map(
            lambda operator_terms: reduce(
                lambda left, tail: tail[0](left, tail[1]),
                operator_terms,
                term,
            )
        )
    )


product = infix(STAR.or_(SLASH), unary)
sum = infix(PLUS.or_(MINUS), product)
comparison = infix(EQUAL.or_(NOT_EQUAL), sum)
expression.parse = comparison.parse  # required for recursive definition


statement: Parser[AST] = error("statement parser used before definition")
return_statement: Parser[AST] = RETURN.and_(expression).bind(
    lambda term: SEMICOLON.and_(constant(ast.Return(term)))
)
expression_statement: Parser[AST] = expression.bind(
    lambda term: SEMICOLON.and_(constant(term))
)
if_statement: Parser[AST] = (
    IF.and_(LEFT_PAREN)
    .and_(expression)
    .bind(
        lambda conditional: RIGHT_PAREN.and_(statement).bind(
            lambda consequence: ELSE.and_(statement).bind(
                lambda alternative: constant(
                    ast.If(conditional, consequence, alternative)
                )
            )
        )
    )
)
while_statement: Parser[AST] = (
    WHILE.and_(LEFT_PAREN)
    .and_(expression)
    .bind(
        lambda conditional: RIGHT_PAREN.and_(statement).bind(
            lambda body: constant(ast.While(conditional, body))
        )
    )
)
var_statement: Parser[AST] = VAR.and_(ID).bind(
    lambda name: ASSIGN.and_(expression).bind(
        lambda value: SEMICOLON.and_(constant(ast.Var(name, value)))
    )
)
assignment_statement: Parser[AST] = ID.bind(
    lambda name: ASSIGN.and_(expression).bind(
        lambda value: SEMICOLON.and_(constant(ast.Assignment(name, value)))
    )
)
block_statement: Parser[ast.Block] = LEFT_BRACE.and_(zero_or_more(statement)).bind(
    lambda statements: RIGHT_BRACE.and_(constant(ast.Block(statements)))
)
parameters: Parser[list[str]] = ID.bind(
    lambda param: zero_or_more(COMMA.and_(ID)).bind(
        lambda params: constant([param, *params])
    )
).or_(constant([]))
function_statement: Parser[AST] = FUNCTION.and_(ID).bind(
    lambda name: LEFT_PAREN.and_(parameters).bind(
        lambda params: RIGHT_PAREN.and_(block_statement).bind(
            lambda block: constant(ast.Function(name, params, block))
        )
    )
)
statement_parser: Parser[AST] = (
    return_statement.or_(function_statement)
    .or_(if_statement)
    .or_(while_statement)
    .or_(var_statement)
    .or_(assignment_statement)
    .or_(block_statement)
    .or_(expression_statement)
)
statement.parse = statement_parser.parse
parser: Parser[AST] = ignored.and_(zero_or_more(statement)).map(
    lambda statements: ast.Block(statements)
)

assert (
    parser.parse_string(
        """

function factorial(n) {
  var result = 1;
  while (n != 1) {
    result = result * n;
    n = n - 1;
  }
  return result;
}

"""
    )
    == ast.Block(
        statements=[
            ast.Function(
                name="factorial",
                params=["n"],
                body=ast.Block(
                    statements=[
                        ast.Var(name="result", value=ast.Number(value=1)),
                        ast.While(
                            conditional=ast.NotEqual(
                                left=ast.Id(value="n"), right=ast.Number(value=1)
                            ),
                            body=ast.Block(
                                statements=[
                                    ast.Assignment(
                                        name="result",
                                        value=ast.Multiply(
                                            left=ast.Id(value="result"),
                                            right=ast.Id(value="n"),
                                        ),
                                    ),
                                    ast.Assignment(
                                        name="n",
                                        value=ast.Subtract(
                                            left=ast.Id(value="n"),
                                            right=ast.Number(value=1),
                                        ),
                                    ),
                                ]
                            ),
                        ),
                        ast.Return(term=ast.Id(value="result")),
                    ]
                ),
            )
        ]
    )
)
