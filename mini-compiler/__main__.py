import sys

from parser import parser
from abstract_syntax_tree import Environment


filename = sys.argv[1]


with open(filename) as f:
    parser.parse_string(f.read()).emit(Environment({}))
